Rails.application.config.action_view.sanitized_allowed_tags = ['strong', 'em', 'a', 'b', 'script']
Rails.application.config.action_view.sanitized_allowed_attributes = ['href', 'title']