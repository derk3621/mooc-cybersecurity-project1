Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'tickets#index'
  get '/index', to: 'index#something'
  get '/output', to: 'index#something2'
  get '/openfile', to: 'index#something3'
  resources :tickets
end
