json.extract! ticket, :id, :name, :ticket_status, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
