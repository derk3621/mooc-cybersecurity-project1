class IndexController < ApplicationController
  def something
    redirect_to(params[:url])
  end

  def something2
    @text = params[:text]
  end

  def something3
    file = params[:file]
    File.open(file, "r") {|f|
      # f.flock(File::LOCK_SH)
      # logger.debug (f.read)
      t = f.read
      logger.debug (t)
      @output = t
    }
    render 'something2'
  end
end
