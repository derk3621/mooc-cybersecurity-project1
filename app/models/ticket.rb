class Ticket < ApplicationRecord
  enum ticket_status: [ :active, :archived, :pending ]
end
