# README

Download it from the url above or do ‘git clone https://gitlab.com/derk3621/mooc-cybersecurity-project1.git’

Then, do a ‘bundle install’ and ‘rails db:setup’ and everything should be installed properly.
Run the server with ‘rails s’.

The database back-end is sqlite3, so no additional database services are needed.
